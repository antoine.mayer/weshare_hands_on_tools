# WeShare hands on tools

## Setup

Install direnv

```
$ sudo apt install direnv
```

For direnv to work properly it needs to be hooked into the shell

```
# BASH
$ echo 'eval "$(direnv hook bash)"' >> ~/.bashrc

# ZSH
$ echo 'eval "$(direnv hook zsh)"' >> ~/.zshrc
```

## Usage

Once the projet cloned, `cd` to the folder `weshare_hands_on_tools` and enter the following command :

```
direnv allow .
```

Wait until the installation of the differents tools

* Linkerd (stable-2.12.1)
* Boundary (boundary_0.11.1)
* K9s (v0.26.7)
* Kubectl (latest)
* Trivy (v0.35.0)

You can confirm that all's good with the `which`command

```
$ which linkerd
/home/amayer/weshare_hands_on_tools/.direnv/bin/linkerd
```
